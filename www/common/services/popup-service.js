(function() {
        'use strict';
        var module = angular.module('services');

        function service($timeout, $ionicPopup) {

            this.showPopup = function(titlebar, subTitlebar, timeout) {
                var myPopup = $ionicPopup.show({
                    template: '',
                    title: titlebar,
                    subTitle: subTitlebar,                    
                    buttons: []
                });
                $timeout(function() {
                    myPopup.close();
                }, timeout);
            }
        }    
    module.service('PopupService', ['$timeout', '$ionicPopup', service]);

})();
