(function() {
    'use strict';
    var module = angular.module('services');

    function service($q, $http, $resource, $cookies, DataSharedService) {
        CB.CloudApp.init('snipetest121', 'kwa3RoA2GUGnZAGjx+ADJVQyWcWr2FVd9acnkj3bS20=');

        function IncreaseLoggedCount(user) {
            var deferred = $q.defer();           
            var count;
            if(user.document.loggedTimes)
            {
                count = user.document.loggedTimes + 1;
            }
            else{
                count = 1;
            }
          
            user.set("loggedTimes", count);
            user.save({
                success: function(user) {
                    deferred.resolve(user);
                },
                error: function(err) {
                    deferred.reject(error);
                }
            });
            return deferred.promise;
        }

        this.login = function(username, password) {
            var user = new CB.CloudUser();
            user.set('username', username);
            user.set('password', password);
            var deferred = $q.defer();
            user.logIn({
                success: function(user) {
                    console.log(user);
                    var currentUser = {
                        "email": user.document.user,
                        "firstname": user.document.firstname,
                        "lastname": user.document.lastname,
                        "mobile": user.document.mobile,
                        "username": user.document.username,
                        "id": user.document._id
                    };
                    window.localStorage.setItem('user', JSON.stringify(currentUser));
                    DataSharedService.SetCustomerId(user.document._id);
                    IncreaseLoggedCount(user).then(function(s) {
                        deferred.resolve(user);
                    }, function(error) {
                        deferred.reject(error);
                    });

                },
                error: function(error) {
                    deferred.reject(error);
                }
            });
            return deferred.promise;
        }


        this.logout = function() {
            var deferred = $q.defer();
            CB.CloudUser.current.logOut({
                success: function(user) {
                    $cookies.remove('user');
                    deferred.resolve(user);
                },
                error: function(error) {
                    deferred.reject(error);
                }
            });
        }

        this.username = function() {
            return username;
        }
        this.role = function() {
            return role;
        }

        this.register = function(object) {
            var deferred = $q.defer();
            var user = new CB.CloudUser();
            user.set('username', object.email);
            user.set('password', object.password);
            user.set('email', object.email);
            user.set('firstname', object.firstname);
            user.set('lastname', object.lastname);
            user.set('mobile', object.mobile);
            user.signUp({
                success: function(user) {
                    deferred.resolve(user);
                    var currentUser = {
                        "email": user.document.user,
                        "firstname": user.document.firstname,
                        "lastname": user.document.lastname,
                        "mobile": user.document.mobile,
                        "username": user.document.username,
                        "id": user.document._id
                    };
                    window.localStorage.setItem('user', JSON.stringify(currentUser));
                },
                error: function(error) {
                    deferred.reject(error);
                }
            });
            return deferred.promise;
        }

        this.updateAccount = function(object) {
            var deferred = $q.defer();
            var query = new CB.CloudQuery("User");
            query.equalTo('username', object.email);
            query.find({
                success: function(list) {
                    list[0].set("firstname", object.firstname);
                    list[0].set("lastname", object.lastname);
                    list[0].set("mobile", object.mobile);
                    if (object.count) {
                        list[0].set("loggedTimes", object.mobile);
                    }
                    list[0].save({
                        success: function(user) {
                            deferred.resolve(user);
                            var currentUser = {
                                "email": user.document.user,
                                "firstname": user.document.firstname,
                                "lastname": user.document.lastname,
                                "mobile": user.document.mobile,
                                "username": user.document.username
                            };
                            window.localStorage.setItem('user', JSON.stringify(currentUser));                           
                        },
                        error: function(err) {
                            deferred.reject(error);
                        }
                    });
                },
                error: function(error) {
                    console.log(error);
                }
            });
            return deferred.promise;
        }

    }
    module.service('AuthService', ['$q', '$http', '$resource', '$cookies', 'DataSharedService', service]);

})();
