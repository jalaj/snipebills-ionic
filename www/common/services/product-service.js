(function() {
    'use strict';
    var module = angular.module('services');

    function service($q) {
        CB.CloudApp.init('snipetest121', 'kwa3RoA2GUGnZAGjx+ADJVQyWcWr2FVd9acnkj3bS20=');
        this.searchProductbyCode = function(barcode) {
            var deferred = $q.defer();

            var cs = new CB.CloudSearch('items');
            cs.searchQuery = new CB.SearchQuery();
            cs.searchQuery.searchOn('barcode', barcode, 0, false,"100%");

          /*  var query = new CB.CloudQuery("items");
            query.equalTo('barcode', barcode);
            query.find({
                success: function(list) {
                    deferred.resolve(list);
                },
                error: function(error) {
                    deferred.resolve(error);
                }
            });*/
              cs.search({
                success: function(list) {
                    deferred.resolve(list);
                },
                error: function(error) {
                    deferred.resolve(error);
                }
            });
            return deferred.promise;
        }
    }
    module.service('ProductService', ['$q', service]);

})();
