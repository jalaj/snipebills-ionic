(function() {
    'use strict';
    var module = angular.module('services');

    function service() {
        var storeValue = "undefined", cartTotal = 0, 
        customerid="", StoreId="",OrderId="", customer={};

        var shoppingCart = {};

        this.SetStore = function SetStore(value) {
            storeValue = value;
        }

        this.GetStore = function GetStore() {
            return storeValue;
        }

        this.SetCart = function (value){
            shoppingCart = value;
        }
        this.GetCart = function (){
            return shoppingCart;
        }

        this.SetCartTotal = function(value){
            cartTotal = value;
        }
        this.GetCartTotal = function(){
           return cartTotal;
        }
        this.SetCustomerId = function(value){
            customerid = value;
        }
         this.GetCustomerId = function(){
            return customerid;
        }

        this.GetCustomer = function(){
            return customer;
        }
        this.SetCustomer = function(value){
            customer = value;
        }

        this.SetStoreId = function(value){
            StoreId = value;
        }
         this.GetStoreId = function(){
            return StoreId;
        }
        this.SetOrderId = function(value){
            OrderId = value;
        }
         this.GetOrderId = function(){
            return OrderId;
        }
    }
    module.service('DataSharedService', [service]);

})();
