(function() {
    'use strict';
    var module = angular.module('services');

    function factory() {
      return  function() {
            var con = navigator.connection.type;
            /*var goodInternet = (con == Connection.CELL_3G ||
                con == Connection.WIFI || con == Connection.CELL_4G ||
                con == Connection.ETHERNET);*/
            var noConection = (con == Connection.NONE);
            if (noConection) {
                return false;
            } else {
                return true;
            }
        }
    }
    module.factory('IsAppOnline', [factory]);

})();
