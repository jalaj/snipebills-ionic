(function() {
        'use strict';
        var module = angular.module('services');


        function service($q, $http, $resource) {
            CB.CloudApp.init('snipetest121', 'kwa3RoA2GUGnZAGjx+ADJVQyWcWr2FVd9acnkj3bS20=');

            this.createOrderId = function(storeid, customerid) {
                var deferred = $q.defer();
                var obj = new CB.CloudObject('orders');
                obj.set('storeid', storeid);
                obj.set('customerid', customerid);
                obj.set('completed', false); //boolean
                obj.set('paid', 0);
                obj.set('totalItems', 0);
                obj.set('disApplied', 0);
                obj.save({
                    success: function(obj) {
                        deferred.resolve(obj);
                        console.log(obj);
                    },
                    error: function(error) {
                        deferred.resolve(error);
                    }
                });
                return deferred.promise;
            };

            this.updateOrder = function(order) {
                var deferred = $q.defer();
                var query = new CB.CloudQuery("orders");
                query.equalTo('_id', order.id);
                console.log(order);
                query.find({
                    success: function(list) {
                        if (order.storeid === list[0].get("storeid") &&
                            order.customerid === list[0].get("customerid")) {
                            list[0].set("paid", order.paid);
                            list[0].set("disApplied", order.disApplied);
                            list[0].set("totalItems", order.totalItems);
                            list[0].set("completed", order.completed);

                            list[0].save({
                                success: function(user) {
                                    deferred.resolve(user);
                                    console.log('order saved');
                                },
                                error: function(err) {
                                    deferred.reject(error);
                                }
                            });
                        } else {
                            deferred.reject();
                        }

                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
                return deferred.promise;
            }
            this.getOrderDetail = function() {}

            this.UpdateOrderDetails = function(itemsarray, orderId) {
                var deferred = $q.defer();
                //  var obj = new CB.CloudObject('orderDetails');
                var position = 0;
                var items = "";
                var objarray = [];                
                angular.forEach(itemsarray, function(selecteditem) {                 
                    var objname = "obj" + position;
                    var objname = new CB.CloudObject('orderdetails');
                    objname.set('itemid', selecteditem.code);
                    objname.set('orderid', orderId);
                    objname.set('price', selecteditem.price);
                 
                    objarray.push(objname);
                    position++;
                });
                console.log(objarray);

                CB.CloudObject.saveAll(objarray, {
                        success: function(res) {
                            console.log(res);
                             deferred.resolve(res);
                        },
                        error: function(err) {
                             deferred.resolve(err);
                        }});

                         return deferred.promise;

                    /*   obj.set('storeid', storeid);
                       obj.set('customerid', customerid);
                       obj.set('completed', false); //boolean
                       obj.set('paid', 0);
                       obj.set('totalItems', 0);
                       obj.set('disApplied', 0);*/
                    /*  obj.save({
                          success: function(obj) {
                              deferred.resolve(obj);
                              console.log(obj);
                          },
                          error: function(error) {
                              deferred.resolve(error);
                          }
                      });*/                   
                }

            }
            module.service('OrderService', ['$q', '$http', '$resource', service]);

        })();
