(function() {
    'use strict';
    var module = angular.module('home');

    function controller($scope, $ionicLoading, $ionicSideMenuDelegate,
        $ionicSlideBoxDelegate, $rootScope, $state, DataSharedService,
        $ionicPlatform, PopupService, ProductService) {

        $scope.home = [];
        var store1 = [],
            store2 = [],
            store3 = [],
            backbutton = 0;
            
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClickable: true,
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: 2500,
            autoplayDisableOnInteraction: false
        });


        store1 = [{
            'home_image': 'img/1.png',
            'name': 'Sign up with adwords',
            'price': ' 121'
        }, {
            'home_image': 'img/2.jpg',
            'name': 'Sign up with adwords',
            'price': ' 122'
        }, {
            'home_image': 'img/3.jpg',
            'name': 'Sign up with adwords',
            'price': ' 323'
        }, {
            'home_image': 'img/4.jpg',
            'name': 'Sign up with adwords',
            'price': ' 212'
        }, {
            'home_image': 'img/5.png',
            'name': 'Sign up with adwords',
            'price': ' 141'
        }];

        store2 = [{
            'home_image': 'img/flowers/1.jpg',
            'name': '20% off on all flowers ',
            'price': ' 543'
        }, {
            'home_image': 'img/flowers/2.jpg',
            'name': '20% off on all flowers ',
            'price': ' 543'
        }, {
            'home_image': 'img/flowers/3.jpg',
            'name': '20% off on all flowers ',
            'price': ' 543'
        }, {
            'home_image': 'img/flowers/4.png',
            'name': '20% off on all flowers ',
            'price': ' 543'
        }, {
            'home_image': 'img/flowers/5.jpg',
            'name': '20% off on all flowers ',
            'price': ' 543'
        }];

        store3 = [{
            'home_image': 'img/house/1.jpg',
            'name': 'Get EMI at 0% intrest ',
            'price': ' 543'
        }, {
            'home_image': 'img/house/2.png',
            'name': 'Get EMI at 0% intrest ',
            'price': ' 543'
        }, {
            'home_image': 'img/house/3.jpg',
            'name': 'Get EMI at 0% intrest ',
            'price': ' 543'
        }, {
            'home_image': 'img/house/4.jpg',
            'name': 'Get EMI at 0% intrest ',
            'price': ' 543'
        }, {
            'home_image': 'img/house/5.jpg',
            'name': 'Get EMI at 0% intrest ',
            'price': ' 543'
        }];

        function storeChanged() {
            $scope.storeNo = $rootScope.value;

            if ($scope.storeNo == "1") {
                $scope.home = store1;
                $ionicSlideBoxDelegate.update();

            } else if ($scope.storeNo == "2") {
                $scope.home = store2;
                $ionicSlideBoxDelegate.update();
            } else {
                $scope.home = store3;
                $ionicSlideBoxDelegate.update();
            }
        }

        $scope.startShopping = function() {
            $state.go('shopping-cart', {}, {
                reload: true
            });
        }
        $ionicPlatform.ready(function() {
            $ionicPlatform.registerBackButtonAction(function(event) {
                if (true) {
                    if ($state.current.name == "menu.home") {
                        if (backbutton == 0) {
                            backbutton++;
                            window.plugins.toast.showShortCenter('Press again to exit');
                            $timeout(function() {
                                backbutton = 0;
                            }, 5000);
                        } else {
                            navigator.app.exitApp();
                        }
                    } else {
                        navigator.app.backHistory();
                    }
                }
            }, 100);
        });

        function _initilize() {
            $rootScope.title = "Snipebills";
            $scope.storeNo === "2";
            storeChanged();
            $scope.items = [];
            $scope.home = store2;
            $ionicSlideBoxDelegate.update();
            // $rootScope.titlebar = false;
            var store = DataSharedService.GetStore();
            if (store !== null && store !== '' && typeof(store) !== 'undefined' &&
                store !== 'undefined') {
                $scope.store = {
                    storeval: store
                };
            }
        }
        _initilize();

    }
    module.controller('HomeCtrl', ['$scope', '$ionicLoading', '$ionicSideMenuDelegate',
        '$ionicSlideBoxDelegate', '$rootScope', '$state', 'DataSharedService',
        '$ionicPlatform', 'PopupService', 'ProductService', controller
    ]);

})();
