(function() {
    'use strict';
    var module = angular.module('register');

    function controller($scope, $state, $ionicPopup, $timeout, $rootScope,
     $ionicHistory, $ionicLoading, AuthService) {
        
        /*$scope.LoadLoginPage = function() {
            console.log('loading previousState page' + $rootScope.previousState);
            $state.go($rootScope.previousState);
        };*/
     /*   $scope.$on('$stateChangeSuccess', function() {
            $ionicLoading.hide();
        });*/

        $scope.previous = function() {
            console.log($ionicHistory);
            $ionicHistory.goBack();
        };

        $scope.register = function(value){
            $ionicLoading.show();
            console.log(value);
            AuthService.register(value).then(function(success){               
                  var myPopup = $ionicPopup.show({
                         template: '',
                         title: 'Registration Successful',
                         subTitle: 'Loading homepage'
                     });
                     $timeout(function() {
                         myPopup.close(); 
                           $state.go('location', {}, {
                         reload: true
                     });
                     }, 2000);
                $ionicLoading.hide();
            },function(error){
                $ionicLoading.hide();
                 var myPopup = $ionicPopup.show({
                         template: '',
                         title: 'Registration Failed',
                         subTitle: 'some error occurred'
                     });
                     $timeout(function() {
                         myPopup.close(); 
                     }, 3000);
                     
                console.log(error);
            });
        }

        $scope.goBack = function() {
            //console.log("called go back");
            navigator.app.backHistory();
        };

        function _initilize() {}
        _initilize();

    }
    module.controller('RegisterCtrl', ['$scope', '$state', '$ionicPopup',
        '$timeout', '$rootScope', '$ionicHistory', '$ionicLoading','AuthService', controller
    ]);

})();
