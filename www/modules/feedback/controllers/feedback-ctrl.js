(function() {
    'use strict';
    var module = angular.module('feedback');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $ionicPlatform, DataSharedService) {

        function _initilize() {         
        }

        $scope.goBack = function() {
            navigator.app.backHistory();
        }
        
        _initilize();
    }
    module.controller('FeedbackCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$ionicPlatform','DataSharedService', controller
    ]);

})();
