(function() {

    'use strict';
    var module = angular.module('login');

    function controller($scope, $state, $ionicPopup, $timeout, $rootScope,
        $ionicPlatform, AuthService, $ionicLoading, IsAppOnline) {
        //$ionicLoading.show();
        /*(AuthService.login('jalaj','jalaj').then(function(result){
            console.log(result);
        }));*/

        /* $scope.$on('$stateChangeSuccess', function() {
             $ionicLoading.hide();
         });*/

        $scope.closeLogin = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };
        /*$rootScope.goBack = function(){
              navigator.app.backHistory();
              $rootScope.titlebar = false;
        }*/

        // Perform the login action when the user submits the login form
        $scope.onTouch = function(data) {
            /* $state.go('location', {}, {
                 reload: true
             });*/
            try {
                $ionicLoading.show();
                console.log('Doing login' + data.username + " " + data.password);
                AuthService.login(data.username, data.password).then(function(result) {                   
                    $ionicLoading.hide();
                    $state.go('location', {}, {
                        reload: true
                    });
                }, function(error) {
                    $ionicLoading.hide();
                    var myPopup = $ionicPopup.show({
                        template: '',
                        title: 'Login failed',
                        subTitle: 'Either username or password is incorrect'
                    });
                    $timeout(function() {
                        myPopup.close(); //close the popup after 3 seconds for some reason
                    }, 3000);
                });
            } catch (e) {
                $ionicLoading.hide();
                var popup = $ionicPopup.show({
                    template: '',
                    title: 'Login failed',
                    subTitle: 'username or password cannot be blank'

                });
                $timeout(function() {
                    popup.close(); //close the popup after 3 seconds for some reason
                }, 2000);
            }
        };

        $scope.register = function() {
            console.log('called register');
            $state.go('register', {}, {
                reload: true
            });
        };

        $scope.resetPassword = function(data) {
            try {
                if (data.username !== '') {
                    var myPopup = $ionicPopup.show({
                        template: '',
                        title: 'Check your mail',
                        subTitle: 'Email has been sent to your Registered email id.'
                    });
                    $timeout(function() {
                        myPopup.close(); //close the popup after 3 seconds for some reason
                    }, 2000);
                };
            } catch (e) {
                var myPopup = $ionicPopup.show({
                    template: '',
                    title: 'Error',
                    subTitle: 'Please enter your username'
                });
                $timeout(function() {
                    myPopup.close(); //close the popup after 3 seconds for some reason
                }, 2000);
            };
        }

        function _initilize() {
            $rootScope.title = "A smarter way to shop is here";
            $rootScope.titlebar = false;
            $scope.data = null;        
        }

        $rootScope.previousState;
        $rootScope.currentState;
        var backbutton = 0;

        $ionicPlatform.ready(function() {
            $ionicPlatform.registerBackButtonAction(function(event) {
                if (true) {
                    if ($state.current.name == "login") {
                        if (backbutton == 0) {
                            backbutton++;
                            window.plugins.toast.showShortCenter('Press again to exit');
                            $timeout(function() {
                                backbutton = 0;
                            }, 5000);
                        } else {
                            navigator.app.exitApp();
                        }
                    } else {
                        navigator.app.backHistory();
                    }
                }
            }, 100);
        });

        _initilize();

        /*$rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
            $rootScope.previousState = from.name;
            $rootScope.currentState = to.name;
            console.log('Previous state:' + $rootScope.previousState)
            console.log('Current state:' + $rootScope.currentState)
        });*/
    }
    module.controller('LoginCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicPlatform', 'AuthService', '$ionicLoading',
        'IsAppOnline', controller
    ]);

})();
