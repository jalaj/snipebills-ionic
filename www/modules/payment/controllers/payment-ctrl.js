(function() {
    'use strict';
    var module = angular.module('payment');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicLoading) {
        function _initilize() {
            $ionicLoading.show();
        }
        $timeout(function() {
            $state.go('invoice', {}, {
                reload: true
            });
            $ionicLoading.hide();
        }, 500);

        $scope.goBack = function() {
            navigator.app.backHistory();
        }
        _initilize();
    }
    module.controller('PaymentCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicLoading', controller
    ]);

})();
