(function() {
    'use strict';
    var module = angular.module('account').
    controller('AccountCtrl', controller);

    controller.$inject = ['$scope', '$state', '$ionicPopup','$timeout', '$rootScope', '$ionicHistory',
     'AuthService','$ionicLoading', '$ionicPopover', '$ionicPlatform'];

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, AuthService, $ionicLoading, $ionicPopover,
        $ionicPlatform) {

        function _initilize() {
            var currentUser;
            $scope.isFirstNamedisabled = true;
            $scope.isLastNamedisabled = true;
            $scope.isMobileDisabled = true;
            $scope.isUpdateBtnHidden = true;
            var user = window.localStorage.getItem('user');
            if (user !== null && user !== '' && typeof(user) !== 'undefined') {
                currentUser = JSON.parse(user);
                $scope.Object = {};
                $scope.Object.email = currentUser.username;
                $scope.Object.mobile = currentUser.mobile;
                $scope.Object.firstname = currentUser.firstname;
                $scope.Object.lastname = currentUser.lastname;
            }
        }

        var template = '<ion-popover-view style="height:50px">' +
            '   <ion-content class="padding" ng-click="editAccount()">' +
            '       Edit Account' +
            '   </ion-content>' +
            '</ion-popover-view>';

        $scope.popover = $ionicPopover.fromTemplate(template, {
            scope: $scope
        });

        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };

        $scope.editAccount = function() {
            $scope.isFirstNamedisabled = false;
            $scope.isLastNamedisabled = false;
            $scope.isMobileDisabled = false;
            $scope.isUpdateBtnHidden = false;
            $scope.popover.hide();
        };

        $scope.update = function(value) {
            $ionicLoading.show();
            AuthService.updateAccount(value).then(function() {
                var myPopup = $ionicPopup.show({
                    template: '',
                    title: 'Update Successful',
                    subTitle: 'All changes got saved'
                });
                $timeout(function() {
                    myPopup.close();
                    $state.go('menu.home', {}, {
                        reload: true
                    });
                }, 2000);
                $ionicLoading.hide();
            }, function() {
                var myPopup = $ionicPopup.show({
                    template: '',
                    title: 'Update Failed',
                    subTitle: 'some error occurred'
                });
                $timeout(function() {
                    myPopup.close();
                }, 3000);
                $ionicLoading.hide();

            });
        }

        $ionicPlatform.ready(function() {
            $ionicPlatform.registerBackButtonAction(function(event) {
                if (true) {
                    if ($state.current.name == "menu.home") {
                        if (backbutton == 0) {
                            backbutton++;
                            window.plugins.toast.showShortCenter('Press again to exit');
                            $timeout(function() {
                                backbutton = 0;
                            }, 5000);
                        } else {
                            navigator.app.exitApp();
                        }
                    } else {
                        navigator.app.backHistory();
                    }
                }
            }, 100);
        });
        
        _initilize();

    }
   

})();
