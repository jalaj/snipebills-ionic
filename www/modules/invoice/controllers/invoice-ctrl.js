(function() {
    'use strict';
    var module = angular.module('invoice');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $ionicPlatform, DataSharedService) {

        function _initilize() {
            $scope.scannedItems = DataSharedService.GetCart();
            var user = window.localStorage.getItem('user');
            if (user !== null && user !== '' && typeof(user) !== 'undefined') {
                var currentUser = JSON.parse(user);
                $scope.Object = {};
                $scope.Object.email = currentUser.username;
                $scope.Object.mobile = currentUser.mobile;
                $scope.Object.firstname = currentUser.firstname;
                $scope.Object.lastname = currentUser.lastname;
            }
            $scope.date = moment().format('L');

             var totalItems = 0;
            angular.forEach($scope.scannedItems, function(selecteditem) {
                totalItems = totalItems + selecteditem.count;
            });

            $scope.totalItems = totalItems;
            $scope.total = DataSharedService.GetCartTotal();
            $scope.orderId = DataSharedService.GetOrderId();
            $scope.discount = "0";
        }
        $ionicPlatform.ready(function() {
            $ionicPlatform.registerBackButtonAction(function(event) {
                if (true) {

                    if ($state.current.name == "login") {
                        if (backbutton == 0) {
                            backbutton++;
                            window.plugins.toast.showShortCenter('Press again to exit');
                            $timeout(function() {
                                backbutton = 0;
                            }, 5000);
                        } else {
                            navigator.app.exitApp();
                        }
                    } else {
                        $state.go('menu.home', {}, {
                            reload: true
                        });
                    }

                }
            }, 100);
        });

        $scope.goBack = function() {
            $state.go('menu.home', {}, {
                    reload: true
                });
        }
        _initilize();
    }
    module.controller('InvoiceCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$ionicPlatform', 'DataSharedService', controller
    ]);

})();
