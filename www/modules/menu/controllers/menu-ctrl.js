(function() {
    'use strict';
    var module = angular.module('menu');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $cookies, DataSharedService) {
        $state.forceReload();

        function _initilize() {
            $scope.store = $rootScope.store;
            var currentUser;
            var user = window.localStorage.getItem('user');
            if (user !== null && user !== '' && typeof(user) !== 'undefined') {
                currentUser = JSON.parse(user);
                DataSharedService.SetCustomer(currentUser);
                $scope.user = currentUser.username;
                $scope.mobileno = currentUser.mobile;
                console.log(currentUser);
            } else {
                $scope.user = "Not Logged in";
                $scope.mobileno = "Please Log in";
            }
        }

        $scope.account = function() {
            $state.go('account', {}, {
                reload: true
            });
        }
        $scope.OrderHistory = function() {
            $state.go('order.present', {}, {
                reload: true
            });
        }
        $scope.changeLocation = function() {
            $state.go('location', {}, {
                reload: true
            });
        }
        $scope.feedback = function() {
            $state.go('feedback', {}, {
                reload: true
            });
        }
        $scope.faqs = function() {
             $state.go('faqs', {}, {
                reload: true
            });
        }
        $scope.contactus = function() {

        }
        $scope.logout = function() {
            $ionicPopup.confirm({
                title: 'Logout',
                template: 'Are you sure you want to Logout?'
            }).then(function(res) {
                if (res) {
                    $state.go('login', {}, {
                        reload: true
                    });
                    window.localStorage.clear();
                    window.localStorage.removeItem('area');
                    window.localStorage.removeItem('city');
                    window.localStorage.removeItem('store');
                    window.localStorage.removeItem('user');
                }
            });
        }

        _initilize();
    }
    module.controller('MenuCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$cookies', 'DataSharedService', controller
    ]).config(function($provide) {
        $provide.decorator('$state', function($delegate, $stateParams) {
            $delegate.forceReload = function() {
                return $delegate.go($delegate.current, $stateParams, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            };
            return $delegate;
        });
    });

})();
