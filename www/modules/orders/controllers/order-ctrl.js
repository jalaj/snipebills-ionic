(function() {
    'use strict';
    var module = angular.module('order');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicLoading) {
        function _initilize() {
          $scope.scannedItems = [{
                "code": "116037889",
                "name": "product product product product",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037888",
                "name": "product 2",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037887",
                "name": "product 3",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037886",
                "name": "product 4",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037885",
                "name": "product 5",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037884",
                "name": "product 6",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            },{
                "code": "116037889",
                "name": "product product product product",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037888",
                "name": "product 2",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037887",
                "name": "product 3",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037886",
                "name": "product 4",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037885",
                "name": "product 5",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }, {
                "code": "116037884",
                "name": "product 6",
                "count": 1,
                "quantity": "2kg",
                "price": 100
            }];
        }       

        $scope.goBack = function() {
            navigator.app.backHistory();
        }
        _initilize();
    }
    module.controller('OrderDetailCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicLoading', controller
    ]);

})();
