(function() {
    'use strict';
    var module = angular.module('order');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicLoading) {

        $scope.goBack = function() {
            navigator.app.backHistory();
        }
          $scope.openInvoice = function(data){
            console.log(data);
             $state.go('vieworder', {}, {
                reload: true
            });             
        }

        function _initilize() {
            $scope.scannedItems = [{
                "id": "116037889",
                "name": "product product product product",
                "count": 1,
                "date": "7 dec 2015",
                "price": 100
            }, {
                "id": "116037888",
                "name": "product 2",
                "count": 1,
                "date": "8 dec 2015",
                "price": 100
            }, {
                "id": "116037887",
                "name": "product 3",
                "count": 1,
                "date": "7 dec 2015",
                "price": 100
            }, {
                "id": "116037886",
                "name": "product 4",
                "count": 1,
                "date": "15 dec 2015",
                "price": 100
            }, {
                "id": "116037885",
                "name": "product 5",
                "count": 1,
                "date": "10 dec 2015",
                "price": 100
            }, {
                "code": "116037884",
                "name": "product 6",
                "count": 1,
                "date": "8 dec 2015",
                "price": 100
            }];
        }

        $scope.goBack = function() {
            navigator.app.backHistory();
        }
        _initilize();
    }
    module.controller('PastOrderCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicLoading', controller
    ]);

})();
