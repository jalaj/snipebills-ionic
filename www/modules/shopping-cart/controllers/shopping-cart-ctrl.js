(function() {
    'use strict';
    var module = angular.module('shopping-cart');

    function controller($scope, $http, $ionicLoading, $ionicPlatform, $timeout,
        $rootScope, $state, $cordovaVibration, DataSharedService,
        OrderService, ProductService, PopupService) {
        $scope.scannedItems = [];
        var backbutton = 0;
        $ionicLoading.show();
        /* $scope.$on('$stateChangeSuccess', function() {
             $ionicLoading.hide();
         });*/

        function GetItemInfobyBarcode(barcode) {
            var item = {};
            item.name = {};
            item.price = {};
            item.desc = {};
            item.count = 1;
            if (barcode === '8904064463059') {
                item.code = '8904064463059';
                item.name = 'bilt Matrix';
                item.price = '92';
                item.desc = 'Premium Notebooks';
                return item;
            } else if (barcode === '8904064471887') {
                item.code = '8904064471887';
                item.name = 'bilt Matrix small Notebooks';
                item.price = '20';
                item.desc = 'Premium Notebooks';
                return item;
            } else if (barcode === '9780143419716') {
                item.code = '9780143419716';
                item.name = 'Social by Ankit Fadia';
                item.price = '150';
                item.desc = '50 ways to improve your professional life';
                return item;
            } else if (barcode === '9781444798845') {
                item.code = '9781444798845';
                item.name = 'The One Thing by Gary Keller';
                item.price = '200';
                item.desc = 'The surprisingly simple truth behind extraordinary results';
                return item;
            } else if (barcode === '116037889') {
                item.code = '116037889';
                item.name = 'Sudexo';
                item.price = '50';
                item.desc = 'Sudexo coupons';
                return item;
            } else {
                return null;
            }
        }

        $scope.goBack = function() {

            if ($scope.scanningActive) {
                scanner.stopScanner();
                $scope.scanningActive = false;
            }
            navigator.app.backHistory();
        }

        function addItemonScreen(item) {
            var count = 1,
                position = -1,
                keepgoing = true,
                itemPosition;
            try {
                angular.forEach($scope.scannedItems, function(selecteditem) {
                    position = position + 1;
                    if (selecteditem.code === item.code && keepgoing) {
                        keepgoing = false;
                        item.count = selecteditem.count + 1;
                        itemPosition = position;
                        selecteditem.count = item.count;
                    }
                });

            } catch (e) {
                alert("error");
            }
            try {
                if (item.count === 1) {                   
                    $scope.scannedItems.push(item);
                } else {
                    // alert($scope.scannedItems.length + "  second loop added");
                    $scope.scannedItems.splice(itemPosition, 1);
                    $scope.scannedItems.push(item);
                }
                var totalPrice = 0;
                angular.forEach($scope.scannedItems, function(selecteditem) {
                    totalPrice = totalPrice + selecteditem.count * selecteditem.price;
                });
               /* PopupService.showPopup("product count",
                    "Count: "+ totalPrice, 4000);*/
                $scope.cartTotal = totalPrice;

                } catch (e) {
                PopupService.showPopup("Error",
                    "happened while counting", 2000);
            }
        }

        $ionicPlatform.ready(function() {
            function cancel() {
                $scope.scanningActive = false;
                cordova.exec(null, null, "ScanditSDK", "cancel", []);
            }

            function SearchDisplayItemByCode(code) {

                ProductService.searchProductbyCode(code).then(function(result) {
                    var ItemInfo = null;
                    if (result.length > 0) {
                        ItemInfo = {
                            "name": result[0].document.name,
                            "code": result[0].document.barcode,
                            "price": result[0].document.price,
                            "desc": result[0].document.description,
                            "count": 1
                        };
                    } else {
                        ItemInfo = {
                            "name": "Name not available",
                            "code": code,
                            "price": 100,
                            "desc": code,
                            "count": 1
                        };
                    }
                    addItemonScreen(ItemInfo);

                    PopupService.showPopup(ItemInfo.name + ' got added',
                        'Price: ' + ItemInfo.price + ' Description: ' + ItemInfo.desc, 2000);
                    $scope.scanbtn = 'closed';

                }, function(error) {
                    console.log(error);

                });
            }

            function success(resultArray) {
                cancel();
                $scope.codeType = resultArray[1];
                $scope.code = resultArray[0];
                SearchDisplayItemByCode(resultArray[0]);
            }

            function failure(error) {
                $scope.scanningActive = false;
            }

            function scanditscanning() {
                try {
                    $scope.scanningActive = true;
                    // See below for all available options.
                    cordova.exec(success, failure, "ScanditSDK", "scan", ["kHWrbccZ+kj+XvKbAKYF9nuHwfER6FJb/d9J+IVh0HI", {
                        "beep": true,
                        "code128": false,
                        "dataMatrix": false,
                        "codeDuplicateFilter": 1000,
                        "continuousMode": false,
                        "portraitMargins": "20/100/20/50"
                    }]);
                } catch (e) {
                    alert('scandit problem');
                }
            };

            function startScanning() {
                /*  if ($scope.storeNo === "1") {
                      try {
                          scanditscanning();
                      } catch (e) {
                          PopupService.showPopup(item.name + ' got added',
                              'Price: ' + item.price + ' Description: ' + item.desc, 2000);
                      }
                  }
                   else*/
                function mantenaScanCompleted(result) {
                    try {
                        $cordovaVibration.vibrate(500);
                        // $scope.codeType = resultArray[1];
                        SearchDisplayItemByCode(result.code);
                        scanner.stopScanner();
                    } catch (e) {
                        alert("error" + e);
                    }
                }
                //BarcodeScanner.MWBstartScanning(callFunc, x, y, width, height)
                scanner.startScanning(MWBSInitSpace.init,
                    mantenaScanCompleted, 3, 9, 94, 40);
                $scope.scanningActive = true;
            }

            $ionicPlatform.registerBackButtonAction(function(event) {
                if (true) {
                    if ($scope.scanningActive) {
                        if ($scope.storeNo === "1") {
                            cancel();
                        } else {
                            scanner.stopScanner();
                            $scope.scanningActive = false;
                        }
                    } else {
                        if ($state.current.name == "menu.home") {
                            if (backbutton == 0) {
                                backbutton++;
                                window.plugins.toast.showShortCenter('Press again to exit');
                                $timeout(function() {
                                    backbutton = 0;
                                }, 5000);
                            } else {
                                navigator.app.exitApp();
                            }
                        } else {
                            //$scope.scannedItems = [];
                            navigator.app.backHistory();
                        }
                    }
                }
            }, 100);

            $scope.handleCamera = function() {
                console.log('camera started');
                if ($scope.scanningActive) {
                    scanner.stopScanner();
                    $scope.scanningActive = false;
                } else {
                    startScanning();
                }
            }
        });

        $scope.increaseProduct = function(product) {
            console.log("increasing value");
            var count = 1,
                position = -1,
                keepgoing = true,
                itemPosition;
            angular.forEach($scope.scannedItems, function(selecteditem) {
                if (selecteditem.code === product.code && keepgoing) {
                    keepgoing = false;
                    product.count = selecteditem.count + 1;
                    selecteditem.count = product.count;
                }
                //  totalPrice = totalPrice + selecteditem.count * selecteditem.price;
            });
            $scope.cartTotal = $scope.cartTotal + product.price;
        }
        $scope.decreaseProduct = function(product) {
            console.log("decresing value")
            var count = 1,
                keepgoing = true,
                position = -1,
                itemPosition = -1,
                array = [];

            angular.forEach($scope.scannedItems, function(selecteditem) {
                position++;
                if (selecteditem.code === product.code && keepgoing) {
                    keepgoing = false;
                    itemPosition = position;
                    console.log(selecteditem.name + "  " + position);
                    product.count = selecteditem.count - 1;
                    selecteditem.count = product.count;
                } else {
                    array.push(selecteditem);
                }
            });
            if (product.count <= 0) {
                $scope.scannedItems.splice(itemPosition, 1);
                console.log(itemPosition);
                // $scope.scannedItems =  array;
            }
            $scope.cartTotal = $scope.cartTotal - product.price;
        }

        $scope.onDragRight = function() {
            $scope.togglebtn = true;
        }

        $scope.onDragLeft = function() {
            $scope.togglebtn = false;
        }

        function _initilize() {
            $scope.togglebtn = true;
            $scope.scannedItems = [];

            var totalPrice = 0;
            angular.forEach($scope.scannedItems, function(selecteditem) {
                totalPrice = totalPrice + selecteditem.count * selecteditem.price;
            });
            $scope.cartTotal = totalPrice;
            DataSharedService.SetCartTotal($scope.cartTotal);

            var customerid = DataSharedService.GetCustomer().id;
            OrderService.createOrderId('waLv3f3X', customerid).
            then(function(result) {
                    $ionicLoading.hide();
                    window.localStorage.setItem('orderid', result.document._id);
                    DataSharedService.SetOrderId(result.document._id);
                },
                function(error) {
                    PopupService.showPopup("Error",
                        "Something bad happened, we will fix it soon. Try again later.", 2000);
                });
        }

        $scope.checkout = function() {
            DataSharedService.SetCart($scope.scannedItems);
            DataSharedService.SetCartTotal($scope.cartTotal);
            var orderid = DataSharedService.GetOrderId();


            if ($scope.scanningActive) {
                scanner.stopScanner();
                $scope.scanningActive = false;
            }

            if ($scope.scannedItems.length > 0) {
                $ionicLoading.show();
                OrderService.UpdateOrderDetails($scope.scannedItems, orderid).
                then(function(success) {
                        $ionicLoading.hide();
                        console.log("order adding successfull");
                        $state.go('review-cart', {}, {
                            reload: true
                        });
                    },
                    function(error) {
                        $ionicLoading.hide();
                        PopupService.showPopup("Error",
                            "Something bad happened, we will fix it soon. Try again later.", 2000);
                    });
            } else {
                PopupService.showPopup("Error",
                    "Add products in cart by scanning them with pink button.", 2000);
            }

        }
        _initilize();


    }
    module.controller('Shopping-cartCtrl', ['$scope', '$http', '$ionicLoading',
        '$ionicPlatform', '$timeout', '$rootScope', '$state',
        '$cordovaVibration', 'DataSharedService', 'OrderService', 'ProductService', 'PopupService', controller
    ]);

})();
