(function() {
    'use strict';
    var module = angular.module('store');

    function directive($ionicModal, $ionicFilterBar) {

        return {
            /* Only use as <fancy-select> tag */
            restrict: 'E',

            /* Our template */
            templateUrl: 'modules/store/views/store-list.html',

            /* Attributes to set */
            scope: {
                'items': '=',
                /* Items list is mandatory */
                'text': '=',
                /* Displayed text is mandatory */
                'value': '=',
                'valueChangedCallback': "&valueChanged",
                'getCustomTextCallback': "&getCustomText",
                /* Selected value binding is mandatory */
                'callback': '&'
            },
            link: function(scope, element, attrs) {

                /* Default values */
                scope.multiSelect = attrs.multiSelect === 'true' ? true : false;
                scope.allowEmpty = attrs.allowEmpty === 'false' ? false : true;

                /* Header used in ion-header-bar */
                scope.headerText = attrs.headerText || '';

                /* Text displayed on label */
                // scope.text          = attrs.text || '';
                scope.defaultText = scope.text || '';

                /* Notes in the right side of the label */
                scope.noteText = attrs.noteText || '';
                scope.noteImg = attrs.noteImg || '';
                scope.noteImgClass = attrs.noteImgClass || '';
                scope.valueProperty = attrs.valueProperty || "id";
                 scope.textProperty = attrs.textProperty || "text";


                $ionicModal.fromTemplateUrl(
                    'modules/store/views/fancy-select-items.html', {
                        'scope': scope
                    }
                ).then(function(modal) {
                    scope.modal = modal;
                });

                /* Validate selection from header bar */
                scope.validate = function(event) {

                    // Select first value if not nullable
                    if (typeof scope.value == 'undefined' || scope.value == '' || scope.value == null) {
                        if (scope.allowEmpty == false) {
                            console.log(scope.items);
                            scope.value = scope.items[0].id;
                            scope.text = scope.items[0].text;

                            // Check for multi select
                            scope.items[0].checked = true;
                        } else {
                            console.log(scope.defaultText);
                            scope.text = scope.defaultText;
                        }
                    }

                    // Hide modal
                    scope.hideItems();

                    // Execute callback function
                    if (typeof scope.callback == 'function') {
                        scope.callback(scope.value);
                    }
                }

                /* Show list */
                scope.showItems = function(event) {
                    event.preventDefault();
                    scope.modal.show();

                };

                /* Hide list */
                scope.hideItems = function() {
                    scope.modal.hide();
                }
                scope.onValueChanged = function(newValue, oldValue) {
                    scope.text = scope.getText(newValue);
                    console.log(newValue);

                    // Notify subscribers that the value has changed
                    scope.valueChangedCallback({
                        value: newValue
                    });
                };

                /* Destroy modal */
                scope.$on('$destroy', function() {
                    scope.modal.remove();
                });

                /* Validate single with data */
                scope.validateSingle = function(item) {
                    // Set selected text
                    scope.text = item.text;

                    // Set selected value
                    scope.value = item.id;

                    // Hide items
                    scope.hideItems();

                    // Execute callback function
                    if (typeof scope.callback == 'function') {
                        scope.callback(scope.value);
                    }
                }
                scope.getText = function(value) {
                    // Push the values into a temporary array so that they can be iterated through
                    var temp;
                    if (scope.multiSelect) {
                        temp = value ? value : []; // In case it hasn't been defined yet
                    } else {
                        temp = (value === null || (typeof value === "undefined")) ? [] : [value]; // Make sure it's in an array, anything other than null/undefined is ok
                    }
                    var text = "";
                    if (temp.length) {
                        // Concatenate the list of selected items
                        angular.forEach(scope.items, function(item, key) {
                            for (var i = 0; i < temp.length; i++) {
                                if (scope.getItemValue(item) == temp[i]) {
                                    text += (text.length ? ", " : "") + scope.getItemText(item);
                                    break;
                                }
                            }
                        });

                    } else {
                        // Just use the default text
                        text = scope.defaultText;
                    }
                    // If a callback has been specified for the text
                    return scope.getCustomTextCallback({
                        value: value
                    }) || text;
                };

                scope.getItemValue = function(item) {
                    console.log(scope.valueProperty ? item[scope.valueProperty] : item);
                    return scope.valueProperty ? item[scope.valueProperty] : item;
                };

                scope.getItemText = function(item) {
                    return scope.textProperty ? item[scope.textProperty] : item;
                };

                scope.$watch(function() {
                    return scope.value;
                }, scope.onValueChanged, true);
            }
        }
    }
    module.directive('fancySelect', ['$ionicModal', '$ionicFilterBar', directive]);

})();
