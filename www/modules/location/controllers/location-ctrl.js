(function() {
    'use strict';
    var module = angular.module('location');

    function controller($scope, $state, $rootScope,
        $timeout, $cookies, DataSharedService, $stateParams, $http) {

        var storeTestStarter = false;
        var areaTestStarter = false;
        var cityTestStarter = false;

        function _initilize() {
            $scope.isContinueEnabled = false;
            $scope.showStoreSelector = false;
            $scope.showAreaSelector = false;
            $scope.LoadSavedLocation();

            $http.get('modules/location/data/location-data.json')
                .then(function(res) {
                    $scope.locationData = res.data;
                    $scope.cities = [];
                    angular.forEach($scope.locationData, function(selecteditem) {
                        var city = {
                            id: selecteditem.cityId,
                            text: selecteditem.name,
                            checked: false
                        };
                        $scope.cities.push(city);
                    });
                });

            $scope.storelist = true;
            $rootScope.titlebar = true;
            $scope.citySelectedValue = "";
            $scope.areaSelectedValue = "";
            $scope.storeSelectedValue = "";
            $scope.disableContinueBtn = true;

            /* $scope.stores = [{
                 id: 1,
                 "text": "Ratnadeep, kondapur",
                 checked: false
             }, {
                 id: 2,
                 "text": "Heritage, kondapur",
                 checked: false
             }, {
                 id: 3,
                 "text": "SPAR, Kothaguda",
                 checked: false
             }, {
                 id: 4,
                 "text": "Ganshyam, Hitech city",
                 checked: false
             }];*/
            /*
                        $scope.areas = [{
                            id: 1,
                            "text": "kondapur",
                            checked: false
                        }, {
                            id: 2,
                            "text": "Jubliee Hills",
                            checked: false
                        }, {
                            id: 3,
                            "text": "Banjara Hills",
                            checked: false
                        }, {
                            id: 4,
                            "text": "Tolichowki",
                            checked: false
                        }];*/

            /*   $scope.cities = [{
                   id: 1,
                   text: 'Hyderabad',
                   checked: false
               }, {
                   id: 2,
                   text: 'Bangalore',
                   checked: false
               }];*/
        }

        $scope.onValueChanged = function(value) {
            console.log(value);
        }

        var filterBarInstance;

        function getItems() {
            var items = [];
            items = [{
                "text": "Ratnadeep, kondapur"
            }, {
                "text": "Heritage, kondapur"
            }, {
                "text": "SPAR, Hitech city"
            }, {
                "text": "Ganshyam, Hitech city"
            }];
            $scope.items = items;
        }
        $scope.refreshItems = function() {
            if (filterBarInstance) {
                filterBarInstance();
                filterBarInstance = null;
            }

            $timeout(function() {
                getItems();
                $scope.$broadcast('scroll.refreshComplete');
            }, 1000);
        };

        $scope.storeOption = function() {
            $('#card2').css('height', '30%');
            console.log('option selected');
            $scope.continueBtn = false;
        }



        $scope.goBack = function() {
            navigator.app.backHistory();
        }

        function LoadAreaValues(value) {
            angular.forEach($scope.locationData, function(selecteditem) {
                if (selecteditem.name === value) {
                    $scope.selectedCity = selecteditem;
                    angular.forEach(selecteditem.areas, function(selectedArea) {
                        var area = {
                            id: selectedArea.areaId,
                            text: selectedArea.name,
                            checked: false
                        };
                        $scope.areas.push(area);
                    });
                }
            });
        }

        function LoadStoreValue(value) {
            angular.forEach($scope.selectedCity.areas, function(selecteditem) {
                if (selecteditem.name === value) {
                    angular.forEach(selecteditem.stores, function(selectedStore) {
                        console.log('my area' + selectedStore);
                        var store = {
                            id: selectedStore.areaId,
                            text: selectedStore.name,
                            checked: false
                        };
                        $scope.stores.push(store);
                    });
                }
            });
        }

        $scope.citySelected = function(value) {
            $scope.city = value;
            $scope.areas = [];

            if (cityTestStarter) {
                LoadAreaValues(value);
                window.localStorage.setItem('city', value);
                $scope.showAreaSelector = true;
            }
            cityTestStarter = true;
        }

        $scope.areaSelected = function(value) {
            $scope.area = value;
            $scope.stores = [];

            if (areaTestStarter) {
                LoadStoreValue(value);
                window.localStorage.setItem('area', value);

                $scope.showStoreSelector = true;
            }
            areaTestStarter = true;
        }

        $scope.storeSelected = function(value) {
            if (storeTestStarter) {

                DataSharedService.SetStore(value);
                window.localStorage.setItem('store', value);

                $scope.store = "";
                $scope.isContinueEnabled = true;
            }
            storeTestStarter = true;
        }
        $scope.loadHome = function() {
            /* $state.transitionTo('menu.home', $stateParams, {
                 reload: true,
                 inherit: false,
                 notify: true
             });*/ //login/:experience_id/:context',{experience_id:myIdNumber,context:'login'
            /*    $state.transitionTo('menu.home', {}, {
                    'reload': true
                });*/
            $state.go('menu.home', {}, {
                'reload': true
            });
        }
        $scope.LoadSavedLocation = function() {
            try {
                var city = window.localStorage.getItem('city');
                var area = window.localStorage.getItem('area');
                var store = window.localStorage.getItem('store');
                if ((city !== null && city !== '' && typeof(store) !== 'undefined' &&
                        city !== 'undefined') && (store !== null && store !== '' && typeof(store) !== 'undefined' &&
                        store !== 'undefined') && (area !== null && area !== '' && typeof(area) !== 'undefined' &&
                        area !== 'undefined')) {
                    $scope.cityvalue = city;
                    $scope.areavalue = area;
                    $scope.storevalue = store;
                    DataSharedService.SetStore(store);
                    $scope.showAreaSelector = true;
                    $scope.showStoreSelector = true;
                    $scope.isContinueEnabled = true;
                    /*LoadAreaValues($scope.areavalue);
                    LoadStoreValue($scope.storevalue);*/
                } else {
                    $scope.cityvalue = "Select City";
                    $scope.areavalue = "Select Area";
                    $scope.storevalue = "Select Store";
                }

            } catch (e) {
                $scope.cityvalue = "Select City";
                $scope.areavalue = "Select Area";
                $scope.storevalue = "Select Store";
            }
        }
        _initilize();
    }
    module.controller('LocationCtrl', ['$scope', '$state', '$rootScope',
        '$timeout', '$cookies', 'DataSharedService', '$stateParams', '$http', controller
    ]);

})();
