(function() {
    'use strict';
    var module = angular.module('splash');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $ionicPlatform, DataSharedService) {

        function _initilize() {
            var store = window.localStorage.getItem('store');
            var user = window.localStorage.getItem('user');

            if (user !== null && user !== '' && typeof(user) !== 'undefined') {

                if (store == null || store == '' || typeof(store) == 'undefined' ||
                    store == 'undefined') {
                    DataSharedService.SetCustomerId(user.id);
                    console.log('SetCustomerId success' + JSON.parse(user).id);
                    $timeout(function() {
                        $state.go('location', {}, {
                            reload: true
                        });
                    }, 2000);
                } else {
                    $timeout(function() {
                        DataSharedService.SetStore(store);
                        DataSharedService.SetCustomerId(user);
                        console.log('SetCustomerId success' + JSON.parse(user).id);
                        $state.go('menu.home', {}, {
                            reload: true
                        });
                    }, 2000);
                }
            } else {
                $timeout(function() {
                    $state.go('login', {}, {
                        reload: true
                    });
                }, 2000);
            }
        }

        _initilize();
    }
    module.controller('SplashCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$ionicPlatform', 'DataSharedService', controller
    ]);

})();
