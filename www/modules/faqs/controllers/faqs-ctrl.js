(function() {
    'use strict';
    var module = angular.module('faqs');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $ionicPlatform, DataSharedService) {

        function _initilize() {         
        }

        $scope.goBack = function() {
            navigator.app.backHistory();
        }

        $scope.hideShow = function(value){
           var a =  value.show == 0 ? 1 : 0;
           value.show = a;
        }
        $scope.faqs =[{
            "q": "How do I pay?",
            "a": "We currently have signed up with test merchant and we are expanding our services very fast. If you would like to know when we are available in your area? just drop a mail to us.",
            "show": 1
        },{
            "q": "Where can I use?",
            "a": "You can use SnipeBills where ever you see our logo inside retail stores. If you wish to find our affilated patners. You can get list from here."
            ,"show": 1
        },{
            "q": "How do I show proof of payment?",
            "a": "A receipt will be generated as soon as you pay online on our app."
            ,"show": 1
        },{
            "q": "Can you query a transaction on my account?",
            "a": "Yes, you can query all transactions in orders tab."
            ,"show": 1
        },{
            "q": "Will SnipeBills work if there is no internet access?",
            "a": "No, currently we dont support that feature. But we are providing free wifi at our merchant patners."
            ,"show": 1
        },{
            "q": "Is SnipeBills safe?",
            "a": "Yes, SnipeBills is completely safe and we will ensure that we protect information of our customers."
            ,"show": 1
        },{
            "q": "Why should I use SnipeBills rather than my card?",
            "a": "We are cashless and fast payment service providers. So it is super easy to shop with our app and checking out could never be so easy."
            ,"show": 1
        },{
            "q": "Who has access to my info?",
            "a": "Only you have access to the information you have saved in our app. We don't disclose your personal and secured information with anyone else."
            ,"show": 1
        },{
            "q": "Can merchants charge me after making a payment to SnipeBills?",
            "a": "No, you will be only charged when you shop at one of our merchant patners. Only when you authorise merchant then he can debit amount from you account."
            ,"show": 1
        }]

        _initilize();
    }
    module.controller('FaqsCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$ionicPlatform','DataSharedService', controller
    ]);

})();
