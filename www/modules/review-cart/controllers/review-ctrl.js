(function() {
    'use strict';
    var module = angular.module('review-cart');

    function controller($scope, $state, $ionicPopup, $timeout,
        $rootScope, $ionicHistory, $cookies, DataSharedService, OrderService, $ionicLoading) {

        function _initilize() {
            $scope.scannedItems = DataSharedService.GetCart();
            $scope.date = moment().format('L');

            $scope.total = DataSharedService.GetCartTotal();
            $scope.discount = "0";
            $scope.mailId = DataSharedService.GetCustomer().username;
            var totalItems = 0;

            angular.forEach($scope.scannedItems, function(selecteditem) {
                totalItems = totalItems + selecteditem.count;
            });
            
            $scope.totalItems = totalItems;
        }

        $scope.goBack = function() {
            navigator.app.backHistory();
        }

        $scope.confirmPayment = function() {
            $ionicLoading.show();
            var order = {
                "id": DataSharedService.GetOrderId(),
                "storeid": "waLv3f3X",
                "customerid": DataSharedService.GetCustomer().id,
                "paid": $scope.total,
                "disApplied": 0,
                "totalItems": $scope.totalItems,
                "completed": true
            };
            OrderService.updateOrder(order).then(function() {
                $ionicLoading.hide();
                $state.go('payment', {}, {
                    reload: true
                });
            }, function() {
                $ionicLoading.hide();
                var myPopup = $ionicPopup.show({
                    template: '',
                    title: 'Error',
                    subTitle: 'Something bad happened, we will fix it soon. Try again later.'
                });
                $timeout(function() {
                    myPopup.close(); //close the popup after 3 seconds for some reason
                }, 2000);
            });
        }
        _initilize();
    }
    module.controller('ReviewCtrl', ['$scope', '$state', '$ionicPopup', '$timeout',
        '$rootScope', '$ionicHistory', '$cookies', 'DataSharedService', 'OrderService', '$ionicLoading', controller
    ]);

})();
