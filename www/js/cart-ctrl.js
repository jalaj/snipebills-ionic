angular.module('controllers', ['ionic', 'services'])

.controller('CartCtrl',['$scope',' $ionicPlatform', '$ionicPopup', '$timeout', 'DatabaseService',
 function($scope, $ionicPlatform, $ionicPopup, $timeout, DatabaseService) {
  
  function SearchCode(barcode) {
    DatabaseService.SearchBarcode(barcode).then(function(list) {
      if (list.length > 0) {
        var item = {};
        item.name = list[0].get('name');
        item.price = list[0].get('price');
        item.desc = list[0].get('desc');
        item.code = list[0].get('barcode');
        item.count = 1;
        console.log(item);
        return item;
      }
      console.log(list);
    }, function() {

    });
  }
console.log( SearchCode('8901233019666'));

   /*     DatabaseService.GetallItems().then(function(list) {
              if (list.length > 0) {
                //alert(list[0].get('name'));
                if (list.length > 0) {
                  console.log(list);
                  for (i = 0; i < list.length; i++){
                    if (list[i].get('barcode') == '8901233018539') {
                      var item = {};
                      item.name = list[i].get('name');
                      item.price = list[i].get('price');
                      item.desc = list[i].get('desc');
                      item.code = list[i].get('barcode');
                      item.count = 1;
                      console.log(item);
                      $scope.ScannedItem = item;
                      addItemonScreen($scope.ScannedItem);
                      $scope.showPopup($scope.ScannedItem);
                    }
                }
              }
            }
          },
          function() {
            alert('error occured');
          });*/

  //console.log(SearchCode('78945621'));
  $scope.scannedItems = []; // [{name:'bilt',price:'212',desc:'124'}];


  function GetItemInfobyBarcode(barcode) {
    var item = {};
    item.name = {};
    item.price = {};
    item.desc = {};
    item.count = 1;

    if (barcode === '8904064463059') {
      item.code = '8904064463059';
      item.name = 'bilt Matrix';
      item.price = '92';
      item.desc = 'Premium Notebooks';
      return item;
    } else if (barcode === '8904064471887') {
      item.code = '8904064471887';
      item.name = 'bilt Matrix small Notebooks';
      item.price = '20';
      item.desc = 'Premium Notebooks';
      return item;
    } else if (barcode === '9780143419716') {
      item.code = '9780143419716';
      item.name = 'Social by Ankit Fadia';
      item.price = '150';
      item.desc = '50 ways to improve your professional life';
      return item;
    } else if (barcode === '9781444798845') {
      item.code = '9781444798845';
      item.name = 'The One Thing by Gary Keller';
      item.price = '200';
      item.desc = 'The surprisingly simple truth behind extraordinary results';
      return item;
    } else if (barcode === '116037889') {
      item.code = '116037889';
      item.name = 'Sudexo';
      item.price = '50';
      item.desc = 'Sudexo coupons';
      return item;
    }
  }

  $ionicPlatform.ready(function() {
      function cancel() {
        cordova.exec(null, null, "ScanditSDK", "cancel", []);
      }

      function addItemonScreen(item) {
        var count = 1,
          position = -1,
          keepgoing = true,
          itemPosition;

        angular.forEach($scope.scannedItems, function(selecteditem) {

          position = position + 1;

          if (selecteditem.code === item.code && keepgoing) {
            keepgoing = false;
            item.count = selecteditem.count + 1;
            itemPosition = position;
          }

        });

        if (item.count === 1) {
          $scope.scannedItems.push(item);
        } else {
          $scope.scannedItems.splice(itemPosition, 1);
          $scope.scannedItems.push(item);
        }
        return count;
      }

      function success(resultArray) {

        // alert("Scanned " + resultArray[0] + " code: " + resultArray[1]);
        cancel();
        $scope.codeType = resultArray[1];
        $scope.code = resultArray[0];
        /*  DatabaseService.SearchBarcode($scope.code).then(function(list) {
            if (list.length > 0) {
              //alert(list[0].get('name'));
              var item = {};
              item.name = list[0].get('name');
              item.price = list[0].get('price');
              item.desc = list[0].get('desc');
              item.code = list[0].get('barcode');
              item.count = 1;
              $scope.ScannedItem = item;
              addItemonScreen($scope.ScannedItem);
              $scope.showPopup($scope.ScannedItem);
            }
          }, function() {
            alert('error occured');
          });*/

        DatabaseService.GetallItems().then(function(list) {
              if (list.length > 0) {
                //alert(list[0].get('name'));
                if (list.length > 0) {
                  for (i = 0; i < list.length; i++){
                    if (list[i].get('barcode') == $scope.code) {
                      var item = {};
                      item.name = list[i].get('name');
                      item.price = list[i].get('price');
                      item.desc = list[i].get('desc');
                      item.code = list[i].get('barcode');
                      item.count = 1;
                      $scope.ScannedItem = item;
                      addItemonScreen($scope.ScannedItem);
                      $scope.showPopup($scope.ScannedItem);
                    }
                }
              }
            }
          },
          function() {
            alert('error occured');
          });

      // $scope.scannedItems.push($scope.ScannedItem);
      // $('#result').html(' <div id="result" style=" padding: 100px;  z-index: 3;font-size: 24px; background: #B3ABB1; font-weight: 500;">' + "Item got scanned. " + resultArray[0] + " Code: " + resultArray[1] + '</div>');
    }

    function failure(error) {
      // alert("Failed: " + error);
    }

    function scan() {
      // See below for all available options.
      cordova.exec(success, failure, "ScanditSDK", "scan", ["yTOjAB6lj2KC50DuQfEl1xpqtcLZ+T3sGhJIrdR8u/A", {
        "beep": true,
        "code128": false,
        "dataMatrix": false,
        "codeDuplicateFilter": 1000,
        "continuousMode": false,
        "portraitMargins": "20/100/20/50"
      }]);
    }
    $scope.scan = function() {
      scan();
    }
    $scope.cancel = function() {
      cancel();
    }

    // 
  });


$scope.showPopup = function(item) {
  $scope.data = {}

  // An elaborate, custom popup
  var myPopup = $ionicPopup.show({
    template: '',
    title: item.name + ' got added',
    subTitle: 'Price: ' + item.price + ' Description: ' + item.desc,
    scope: $scope,
    buttons: []

  });
  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });
  $timeout(function() {
    myPopup.close(); //close the popup after 3 seconds for some reason
  }, 2000);
};



}]);