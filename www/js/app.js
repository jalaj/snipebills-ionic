// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'services', 'login', 'register',
    'menu', 'home', 'location', 'shopping-cart', 'account',
    'review-cart', 'payment', 'invoice', 'order','faqs','feedback','splash'
])

.run(function($ionicPlatform, $rootScope, DataSharedService,
    IsAppOnline, $ionicPopup, $state) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        function onDeviceReady() {
            if (!IsAppOnline()) {
                $ionicPopup.alert({
                        title: "Internet Not Available",
                        template: "App will exit."
                    })
                    .then(function(result) {
                        ionic.Platform.exitApp();
                    });
            }
        }
        document.addEventListener("deviceready", onDeviceReady, false);

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }
    });
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams) {
            /*var store = DataSharedService.GetStore();
            if (store !== null && store !== '' && typeof(store) !== 'undefined' &&
                store !== 'undefined') {
                $rootScope.store = {
                    storeval: store
                };
                console.log(store);
            }*/
        });

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        // Check if the targeted state is the redirection state
        if (toState.name == 'menu.home') {
            // var gotoState = urlManagerService.getState(toParams);
            //  $state.transitionTo('menu.home',toParams, { location:false, inherit:true, reload:true, notify:true });
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
        .state('menu', {
            url: '/menu',
            abstract: true,
            templateUrl: 'modules/menu/views/sidebar.html',
            controller: 'MenuCtrl'
        })
        // setup an abstract state for the tabs directive
        .state('tab', {
            url: '/tab',
            abstract: true,
            templateUrl: 'templates/tabs.html'
        })
        .state('menu.home', {
            url: '/home',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'modules/home/views/home.html',
                    controller: 'HomeCtrl'
                }
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'modules/login/views/login.html',
            controller: 'LoginCtrl'
        })
        .state('account', {
            url: '/account',
            cache: false,
            templateUrl: 'modules/account/views/account.html',
            controller: 'AccountCtrl'
        })
        .state('shopping-cart', {
            url: '/shopping-cart',
            templateUrl: 'modules/shopping-cart/views/shopping-cart.html',
            controller: 'Shopping-cartCtrl',
             cache: false
        })
        .state('review-cart', {
            url: '/review-cart',
            templateUrl: 'modules/review-cart/views/review.html',
            controller: 'ReviewCtrl',
            cache: false
        })
        .state('register', {
            url: '/register',
            templateUrl: 'modules/register/views/register.html',
            controller: 'RegisterCtrl'
        })
        .state('location', {
            url: '/location',
            templateUrl: 'modules/location/views/location.html',
            controller: 'LocationCtrl'
        })
        .state('payment', {
            url: '/payment',
            cache: false,
            templateUrl: 'modules/payment/views/payment.html',
            controller: 'PaymentCtrl',
            cache: false
        })
        .state('invoice', {
            url: '/invoice',
            templateUrl: 'modules/invoice/views/invoice.html',
            controller: 'InvoiceCtrl',
            cache: false
        })

    .state('order', {
            url: "/order",
            abstract: true,
            templateUrl: "modules/orders/views/order-tabs.html"
        })
        .state('order.past', {
            url: "/past",
            views: {
                'orders-past': {
                    templateUrl: "modules/orders/views/order.html",
                    controller: 'PastOrderCtrl'

                }
            }
        })
        .state('order.present', {
            url: "/present",
            views: {
                'orders-present': {
                    templateUrl: "modules/orders/views/order.html",
                    controller: 'RecentOrderCtrl'
                }
            }
        })
        .state('vieworder', {
            url: "/vieworder",
            templateUrl: "modules/invoice/views/invoice.html",
            controller: 'OrderDetailCtrl'
        })
         .state('feedback', {
            url: "/feedback",
            templateUrl: "modules/feedback/views/feedback.html",
            controller: 'FeedbackCtrl'
        })
           .state('faqs', {
            url: "/faqs",
            templateUrl: "modules/faqs/views/faqs.html",
            controller: 'FaqsCtrl'
        })
            .state('splash', {
            url: "/splash",
            templateUrl: "modules/splash/views/splash.html",
            controller: 'SplashCtrl'
        });

    // if none of the above states are matched, use this as the fallback
   $urlRouterProvider.otherwise('/splash');
});
