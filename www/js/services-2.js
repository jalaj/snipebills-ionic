angular.module('ionic-ecommerce.services', [])
.factory('ProductService', ProductService);
// Product Service
ProductService.$inject = ['$http', '$q'];
function ProductService($http, $q) {
  var service = this;
  service.endpoint = 'https://ionic-ecommerce.herokuapp.com/api/products';
  service.all = all;
  service.get = get;

  function all(cache) {
    return httpRequestHandler(service.endpoint, cache);
  }

  function get(slug, cache) {
    return httpRequestHandler(service.endpoint + "/" + slug, cache);
  }

  function httpRequestHandler(url, cache) {
    var timedOut = false,
        timeout = $q.defer(),
        result = $q.defer(),
        httpRequest;

    cache = (typeof cache === 'undefined') ? true : cache;

    setTimeout(function () {
      timedOut = true;
      timeout.resolve();
    }, (1000 * 10));

    httpRequest = $http({
      method: 'get',
      url: url,
      cache: cache,
      timeout: timeout.promise
    });

    httpRequest.success(function(response, status, headers) {
      var data = {};
      data.response = response;
      data.status = status;
      data.headers = headers;
      
      result.resolve(data);
    });

    httpRequest.error(function(rejection, status, headers) {
      var data = {};
      data.status = status;
      data.headers = headers;
      console.log('error occured');
     
      data.rejection = timedOut ? 'Request took longer than ' + '1' + ' seconds.' : rejection;
      result.reject(data);
    });

    return result.promise;
  }
  return service;
}